package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class SecondLabIndiv {
    private WebDriver chromeDriver;
    private static final String baseUrl = "https://www.google.com/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        //Run driver
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        //set fullscreen
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.addArguments("--remote-allow-origins=*");
        //setup wait for loading elements
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(5));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions(){
        //open main page
        chromeDriver.get(baseUrl);
    }

    @Test
    public void testSearchExistAndEmpty(){
        WebElement searchInput = chromeDriver.findElement(By.id("APjFqb"));
        String searchText = "aws";
        Assert.assertNotNull(searchInput);
        searchInput.sendKeys(searchText);
        Assert.assertEquals(searchInput.getAttribute("value"), searchText);
        searchInput.sendKeys(Keys.ENTER);
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), baseUrl);
    }

    @Test
    public void testFindElementByXpath(){
        chromeDriver.get(baseUrl + "search?q=youtube");
        WebElement gridVersions = chromeDriver.findElement(By.xpath("/html/body/div[5]/div/div[9]/div[1]/div[2]/div[2]/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/span/a/h3"));
        Assert.assertNotNull(gridVersions);
    }

    @Test
    public void testLang(){
        WebElement langRuUa = chromeDriver.findElement(By.cssSelector("[lang|=ru-UA]"));
        Assert.assertNotNull(langRuUa);
    }


    @AfterClass(alwaysRun = true)
    public void tearDown(){
        chromeDriver.quit();
    }

}