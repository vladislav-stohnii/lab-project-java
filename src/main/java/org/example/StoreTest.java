package org.example;


import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class StoreTest {

    private static final String baseUrl = "https://petstore.swagger.io/v2";
    private static final int ORDER_ID = 12223121;

    private static final String ORDER = "/store",
            STORE_ORDER = ORDER + "/order",
            STORE_ORDER_ORDERID = STORE_ORDER + "/{orderId}";

    private String username;

    private String firstName;

    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void placeOrder(){
        Map<String, ?> body = Map.of(
                "id", ORDER_ID,
                "petId", 1,
                "quantity", 1,
                "shipDate", "2023-11-18T21:39:25.515Z",
                "status", "placed",
                "complete", true
        );
        Response response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .post(STORE_ORDER);
        response.then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "placeOrder")
    public void findOrder(){
        given()
            .pathParams("orderId", ORDER_ID)
            .get(STORE_ORDER_ORDERID)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .and()
            .body("id", equalTo(ORDER_ID));
    }

    @Test(dependsOnMethods = "findOrder")
    public void deleteOrder(){
        given()
                .pathParams("orderId", ORDER_ID)
                .delete(STORE_ORDER_ORDERID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

}
