package org.example;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class LabThree {

    private static final String baseUrl = "https://9b1b74e0-a54b-448f-afa4-2e24e8548b3a.mock.pstmn.io";

    private static final String GET_SUCCESS = "/ownerName/success",
            GET_UNSUCCESS = "/ownerName/unsuccess",
            POST = "/createSomething",
            PUT = "/updateMe",
            DELETE = "/deleteWorld";

    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void testGetSuccess(){
        given()
                .get(GET_SUCCESS)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testGetUnsuccess(){
        given()
                .get(GET_UNSUCCESS)
                .then()
                .statusCode(HttpStatus.SC_FORBIDDEN);
    }

    @Test
    public void testPostSuccess(){
        given()
                .queryParam("permission", "yes")
                .post(POST)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testPostUnsuccess(){
        given()
                .post(POST)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testPut(){
        Map<String, ?> body = Map.of(
                "name", "Vladyslav",
                "surname", "Stohnii"
        );
        given()
                .body(body)
                .put(PUT)
                .then()
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testDelete() {
        given()
                .delete(DELETE)
                .then()
                .statusCode(HttpStatus.SC_GONE);
    }
}
